//function to get ajax call
$.getJSON("battles.json", function(data){

            const json1=data;
            var most_active={};
            var attacker_outcome={};
            var defender_size={};
            var mapofattackers={};
            var mapofdefenders={};
            var mapofregions={};
            var attacker_outcomes={};
            var mapofuniquebattles={};
            var defender_sizes=[];
            var totalDefendersSize;
            var arr=[];
            var most_attacker_king;
            var most_defender_king;
            var most_active_region;
            var uniquebattles=[];
            var attacker_outcomes_win=0;
            var attacker_outcomes_lost=0;
            var sum=0, min=0, max=0;
            

            //Most active Attacker code
            var attackmap=json1.forEach(function(attacker)
            {
                var name = attacker.attacker_king
                if(mapofattackers[name]==null)
                {
                    mapofattackers[name]=1;
                }
                else
                {
                    mapofattackers[name]++;
                }
            });

            most_attacker_king=Object.keys(mapofattackers).reduce((a, b) => mapofattackers[a] > mapofattackers[b] ? a : b);



            //Most active defender code
            var defendmap=json1.forEach(function(defender)
            {
                var name = defender.defender_king
                if(mapofdefenders[name]==null)
                {
                    mapofdefenders[name]=1;
                }
                else
                {
                    mapofdefenders[name]++;
                }
            });

            most_defender_king=Object.keys(mapofdefenders).reduce((a, b) => mapofdefenders[a] > mapofdefenders[b] ? a : b);


            //Most active region code
            var regionmap=json1.forEach(function(regions)
            {
                var name = regions.region
                if(mapofregions[name]==null)
                {
                    mapofregions[name]=1;
                }
                else
                {
                    mapofregions[name]++;
                }
            });

            most_active_region=Object.keys(mapofregions).reduce((a, b) => mapofregions[a] > mapofregions[b] ? a : b);




            //Win or lost code, 2nd object in required output
            var winmap=json1.forEach(function(wins)
            {
                var name = wins.attacker_outcome;
    
                if(name=="win")
                {
                    attacker_outcomes_win++;
                }
                else if(name!="win" && name!="")
                {
                    attacker_outcomes_lost++;
                }

            });


            //code to find unique battles
            var unique_battles_map=json1.forEach(function(unique)
            {
                var name = unique.battle_type
                if(mapofuniquebattles[name]==null)
                {
                    mapofuniquebattles[name]=1;
                }
                else
                {
                    mapofuniquebattles[name]++;
                }

            });

            uniquebattles=Object.keys(mapofuniquebattles).filter(Boolean);


            //code to save defender size from json to a variable
            var defendersizes=json1.forEach(function(ds)
            {
                var name = ds.defender_size;
                defender_sizes.push(name)
            });

            totalDefendersSize=defender_sizes.filter(x=>x!=null)


            //adding defeder size 
            for(let i=0; i<totalDefendersSize.length; i++)
            {
                sum+=totalDefendersSize[i];
            }

            //getting avg 
            sum=sum/totalDefendersSize.length;

            sum=Math.round(sum) //avg

            min=Math.min(...totalDefendersSize);

            max=Math.max(...totalDefendersSize);


            //required 1st object
            most_active=
            {
                'attacker_king': most_attacker_king,
                'defender_king': most_defender_king,
                'region':most_active_region,
            }

            //required 2nd object
            attacker_outcome=
            {
                'win': attacker_outcomes_win,
                'lost': attacker_outcomes_lost
            }

            //required last object
            defender_size=
            {
                'average':sum,
                'min':min,
                'max':max
            }

            //defining an object to store data in req form
            let obj={
                most_active,
                attacker_outcome,
                uniquebattles,
                defender_size
            }
            //converting to json and printing the output
            console.log(obj);
});

